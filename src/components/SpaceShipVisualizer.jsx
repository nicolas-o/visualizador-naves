import { useState, useEffect } from "react";
import { formatThousandSeparator } from "../utils/formatThousandSeparator";
import "./SpaceShipVisualizer.css";

const SpaceShipVisualizer = () => {
  const [data, setData] = useState();
  const [open, setOpen] = useState(false);
  const [label, setLabel] = useState("Seleccione una nave...");
  const [option, setOption] = useState("Executor");

  const handleToggle = () => {
    setOpen(!open);
  };

  useEffect(() => {
    const getData = async () => {
      let results = [];
      let information = [];
      let page = `https://swapi.dev/api/starships/?page=1`;
      do {
        let response = await fetch(page);
        let data = await response.json();
        results.push(data.results);
        page = data.next;
      } while (page != null);

      results.map((item) => {
        return item.map((info) => {
          return information.push(info);
        });
      });

      setData(information);
    };
    getData();
  }, []);

  if (data !== undefined) {
    return (
      <div className="app">
        <div className="selectionMenu" onClick={handleToggle}>
          <label className="selectionLabel">{label}</label>
          <button type="button" className="polygon"></button>
          <div
            className={
              open === false ? "menuOptions" : "menuOptions menuOptionsActive"
            }
          >
            {data.map((item) => {
              return (
                <option
                  className="shipName"
                  key={item.name}
                  value={item.name}
                  onClick={() => {
                    setLabel(item.name);
                    setOption(item.name);
                    handleToggle();
                  }}
                >
                  {item.name}
                </option>
              );
            })}
          </div>
        </div>
        <div className="infoDisplay">
          {data.map((item) => {
            if (item.name === option) {
              return (
                <div key={item.name}>
                  <h2>{item.name}</h2>
                  <p>{item.model}</p>
                  <hr />
                  <h3>Fabricante</h3>
                  <p>{item.manufacturer}</p>
                  <h3>Largo</h3>
                  <p>{formatThousandSeparator(item.length)} fts.</p>
                  <h3>Valor</h3>
                  <p>
                    {formatThousandSeparator(item.cost_in_credits)} créditos
                  </p>
                  <h3>Cantidad pasajeros</h3>
                  <p>{formatThousandSeparator(item.passengers)}</p>
                </div>
              );
            }
          })}
        </div>
        <div className="passengersDisplay">
          <h2>Pasajeros</h2>
          <hr />
        </div>
      </div>
    );
  } else {
    return <h1 className="loading">Loading...</h1>;
  }
};

export default SpaceShipVisualizer;
