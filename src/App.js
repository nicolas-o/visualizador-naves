import SpaceShipVisualizer from "./components/SpaceShipVisualizer";
import "./App.css";

function App() {
  return (
    <div className="app">
      <SpaceShipVisualizer />
    </div>
  );
}

export default App;
